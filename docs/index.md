# 💫 A beautiful readme for a beautiful article 🗞️

This is a sample readme to show how to make cool GitLab pages with mkdocs.

## 🚀 First chapter

!!! quote "Martin Luther King"
    I've a dream !

### 🤙 A level-2 chapter

??? info "Some sensitive data"
    🤐 This is secret data

## ✌️ Second one

!!! tip "A interesting blog"
    [yodamad's blog](https://yodamad.hashnode.dev/)

## 🎶 With annotations

This a text explaining something about mkdocs (1) ; this text is really interesting !
{ .annotate }

1.  🔗 [mkdocs](https://www.mkdocs.org/) is an opensource project

!!! example annotate
    A cool sample of a stuff (1)

1.  🚨 This is just an example, adapt it...
