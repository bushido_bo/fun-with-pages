# 🧑‍💻 Some code samples with differents behavior

## 🖍️ Highlighting

***No line highlighted***

```yaml
  palette: 
    # Palette toggle for light mode
    - scheme: default
      toggle:
        icon: material/brightness-7 
        name: Switch to dark mode
    # Palette toggle for dark mode
    - scheme: slate
      toggle:
        icon: material/brightness-4
        name: Switch to light mode    
```

***Two blocks highlighted***

```go hl_lines="4-6 14-16"
func main() {
	// Définition de 2 sous-commandes
	helloWorldCmd := &cobra.Command{
		Use:   "hello",
		Short: "Print hello world",
		Long:  `This command will print hello world for demo purpose`,
		Run: func(cmd *cobra.Command, args []string) {
			log.Info().Msg("Hello DevopsDDay !")
		},
	}
	rootCmd.AddCommand(helloWorldCmd)

	startServerCmd := &cobra.Command{
		Use:   "start",
		Short: "Start the server",
		Long:  `This command will start the server with some additionnal configuration`,
		Run: func(cmd *cobra.Command, args []string) {
			setupLogger()

			startupHttpServer()
		},
	}
	rootCmd.AddCommand(startServerCmd)

	service.SetupOpentelemetryService()

	err := rootCmd.Execute()
	if err != nil {
		os.Exit(1)
	}
}
```

***With line numbers***

```java linenums="1"
private fun isCloseTo(pos: Pair<Int, Int>): Pair<Int, Int>? =
	if (pos.first > 0 && lines[pos.second][pos.first-1] == 'S') pos.first-1 to pos.second
	else if (pos.first+1 < lines[0].length && lines[pos.second][pos.first+1] == 'S') pos.first+1 to pos.second
	else if (pos.second > 0 && lines[pos.second-1][pos.first] == 'S') pos.first to pos.second-1
	else if (pos.second+1 < lines.size && lines[pos.second+1][pos.first] == 'S') pos.first to pos.second+1
	else null
```

## 🛟 With annotations

```yaml
theme:
  name: material # (1)
  features:
    - content.code.copy # (2)
```

1. 🎨 Use the material theme
2. 🧬 Copy code option enablement

## 🔮 From a snippet

```yaml
--8<-- ".gitlab-ci.yml"
```
